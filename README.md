# UR Communication

[![Crates.io](https://img.shields.io/crates/v/ur_comm.svg)](https://crates.io/crates/ur_comm) [![Crates.io](https://img.shields.io/crates/d/ur_comm.svg)](https://crates.io/crates/ur_comm) [![license](https://img.shields.io/crates/l/qwerty.svg)](https://gitlab.com/starshell/robotics/ur_comm/blob/master/LICENSE) [![Coverage Status](https://codecov.io/gl/starshell/robotics/ur_comm/branch/master/graph/badge.svg)](https://codecov.io/gl/starshell/robotics/ur_comm)

Linux: [![Build status](https://gitlab.com/starshell/robotics/ur_comm/badges/master/pipeline.svg)](https://gitlab.com/starshell/robotics/ur_comm/commits/master)
Windows: [![Build status](https://ci.appveyor.com/api/projects/status/k7ccce79080tfu18/branch/master?svg=true)](https://ci.appveyor.com/project/Eudoxier/ur_comm/branch/master)

Easy to use library for communication with Universal Robots.

## Usage

Add `ur_comm` as a dependency in your `Cargo.toml` to use from crates.io:

```toml
[dependencies]
ur_comm = "0.1.0"
```

Then add `extern crate ur_comm;` to your crate root and run `cargo build` or `cargo update && cargo build` for your project. Detailed documentation for releases can be found on [docs.rs](https://docs.rs/question/).

## Contributing

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/robotics/ur_comm/issues). Don't have a GitLab account? Just email `incoming+starshell/robotics/ur_comm@gitlab.com` and those emails automatically become issues (with the comments becoming the email conversation).

To contribute to **UR Communication**, please see [CONTRIBUTING](CONTRIBUTING.md).

## License

**UR Communication** is distributed under the terms of the MIT license. If this does not suit your needs for some reason please feel free to contact me, or open an issue.

See [LICENSE](LICENSE).
