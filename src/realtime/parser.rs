//! Parser for Universal Robot messages.

use common::modes::{JointMode, RobotMode, SafetyMode};
use nom::{be_f64, be_i32, IResult};
use realtime::RealtimeRobotState;

struct RobotStateParser {
    buffer: [u8; 1060],
}

impl RobotStateParser {
    pub fn new() -> RobotStateParser {
        let buffer = [0; 1060];
        RobotStateParser { buffer }
    }
}

pub fn parse_robot_state(input: &[u8]) -> IResult<&[u8], RealtimeRobotState> {
    do_parse!(
        input,
        message_size: be_i32
            >> time: be_f64
            >> q_target: count_fixed!(f64, be_f64, 6)
            >> qd_target: count_fixed!(f64, be_f64, 6)
            >> qdd_target: count_fixed!(f64, be_f64, 6)
            >> i_target: count_fixed!(f64, be_f64, 6)
            >> m_target: count_fixed!(f64, be_f64, 6)
            >> q_actual: count_fixed!(f64, be_f64, 6)
            >> qd_actual: count_fixed!(f64, be_f64, 6)
            >> i_actual: count_fixed!(f64, be_f64, 6)
            >> i_control: count_fixed!(f64, be_f64, 6)
            >> tool_vector_actual: count_fixed!(f64, be_f64, 6)
            >> tcp_speed_actual: count_fixed!(f64, be_f64, 6)
            >> tcp_force: count_fixed!(f64, be_f64, 6)
            >> tool_vector_target: count_fixed!(f64, be_f64, 6)
            >> tcp_speed_target: count_fixed!(f64, be_f64, 6)
            >> digital_input_bits: be_f64
            >> motor_temperatures: count_fixed!(f64, be_f64, 6)
            >> controller_timer: be_f64
            >> test_value: be_f64
            >> robot_mode: be_f64
            >> joint_modes: count_fixed!(f64, be_f64, 6)
            >> safety_mode: be_f64
            >> undocumented_one: count_fixed!(f64, be_f64, 6)
            >> tool_accelerometer_values: count_fixed!(f64, be_f64, 3)
            >> undocumented_two: count_fixed!(f64, be_f64, 6)
            >> speed_scaling: be_f64
            >> linear_momentum_norm: be_f64
            >> undocumented_three: be_f64
            >> undocumented_four: be_f64
            >> v_main: be_f64
            >> v_robot: be_f64
            >> i_robot: be_f64
            >> v_actual: count_fixed!(f64, be_f64, 6)
            >> digital_outputs: be_f64
            >> program_state: be_f64 >> (RealtimeRobotState {
            message_size,
            time,
            q_target,
            qd_target,
            qdd_target,
            i_target,
            m_target,
            q_actual,
            qd_actual,
            i_actual,
            i_control,
            tool_vector_actual,
            tcp_speed_actual,
            tcp_force,
            tool_vector_target,
            tcp_speed_target,
            digital_input_bits,
            motor_temperatures,
            controller_timer,
            test_value,
            robot_mode: RobotMode::from_f64(robot_mode),
            joint_modes: convert_joint_modes(joint_modes),
            safety_mode: SafetyMode::from_f64(safety_mode),
            undocumented_one,
            tool_accelerometer_values,
            undocumented_two,
            speed_scaling,
            linear_momentum_norm,
            undocumented_three,
            undocumented_four,
            v_main,
            v_robot,
            i_robot,
            v_actual,
            digital_outputs,
            program_state,
        })
    )
}

fn convert_joint_modes(joint_modes: [f64; 6]) -> [JointMode; 6] {
    [
        JointMode::from_f64(joint_modes[0]),
        JointMode::from_f64(joint_modes[1]),
        JointMode::from_f64(joint_modes[2]),
        JointMode::from_f64(joint_modes[3]),
        JointMode::from_f64(joint_modes[4]),
        JointMode::from_f64(joint_modes[5]),
    ]
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
