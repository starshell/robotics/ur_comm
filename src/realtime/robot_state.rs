//! A realtime robot state message.

use modes::{JointMode, RobotMode, SafetyMode};
use realtime::parser::parse_robot_state;
use std::fmt;

/// Realtime Universal Robot state message.
#[derive(Clone)]
pub struct RealtimeRobotState {
    /// Total message length in bytes.
    pub message_size: i32,
    /// Time elapsed since the controller was started.
    pub time: f64,
    /// Target joint positions.
    pub q_target: [f64; 6],
    /// Target joint velocities.
    pub qd_target: [f64; 6],
    /// Target joint accelerations.
    pub qdd_target: [f64; 6],
    /// Target joint currents.
    pub i_target: [f64; 6],
    /// Target joint moments (torques).
    pub m_target: [f64; 6],
    /// Actual joint positions.
    pub q_actual: [f64; 6],
    /// Actual joint velocities.
    pub qd_actual: [f64; 6],
    /// Actaul joint currents.
    pub i_actual: [f64; 6],
    /// Joint control currents.
    pub i_control: [f64; 6],
    /// Actual Cartesian coordinates of the tool: `(x, y, z, rx, ry, rz)`,
    /// where `rx`, `ry` and `rz` is a rotation vector representation of
    /// the tool orientation.
    pub tool_vector_actual: [f64; 6],
    /// Actual speed of the tool given in Cartesian coordinates.
    pub tcp_speed_actual: [f64; 6],
    /// Generalised forces in the TCP.
    pub tcp_force: [f64; 6],
    /// Target Cartesian coordinates of the tool: `(x, y, z, rx, ry, rz)`,
    /// where `rx`, `ry` and `rz` is a rotation vector representation of
    /// the tool orientation.
    pub tool_vector_target: [f64; 6],
    /// Target speed of the tool given in Cartesian coordinates.
    pub tcp_speed_target: [f64; 6],
    /// Current state of the digital inputs.
    ///
    /// **NOTE**: These are bits encoded as `int64_t`, e.g. a value of 5 corresponds
    /// to bit 0 and bit 2 set high.
    pub digital_input_bits: f64,
    /// Temperature of each joint in degrees celsius.
    pub motor_temperatures: [f64; 6],
    /// Controller realtime thread execution time.
    pub controller_timer: f64,
    /// A value used by Universal Robots software only.
    pub test_value: f64,
    /// Robot mode data.
    pub robot_mode: RobotMode,
    /// Joint mode data for each joint.
    pub joint_modes: [JointMode; 6],
    /// Safety mode data.
    pub safety_mode: SafetyMode,
    /// Undocumented and used by Universal Robots software only.
    pub undocumented_one: [f64; 6],
    /// Tool `x`, `y` and `z` accelerometer values (software version 1.7).
    pub tool_accelerometer_values: [f64; 3],
    /// Undocumented and used by Universal Robots software only.
    pub undocumented_two: [f64; 6],
    /// Speed scaling of the trajectory limiter.
    pub speed_scaling: f64,
    /// Norm of Cartesian linear momentum.
    pub linear_momentum_norm: f64,
    /// Undocumented and used by Universal Robots software only.
    pub undocumented_three: f64,
    /// Undocumented and used by Universal Robots software only.
    pub undocumented_four: f64,
    /// Masterboard: Main voltage.
    pub v_main: f64,
    /// Masterboard: Robot voltage (48V).
    pub v_robot: f64,
    /// Masterboard: Robot current.
    pub i_robot: f64,
    /// Actual joint voltages.
    pub v_actual: [f64; 6],
    /// Digital outputs.
    pub digital_outputs: f64,
    /// Program state.
    pub program_state: f64,
}

impl RealtimeRobotState {
    pub fn from_packet(packet: &[u8]) -> RealtimeRobotState {
        parse_robot_state(packet)
            .expect("Failed to parse robot state")
            .1
    }
}

macro_rules! write_joints {
    ($title:expr, $f:expr, $joint_names:expr, $joint_values:expr) => {
        writeln!($f, "\t{}:", $title)?;
        for (joint_name, joint_value) in $joint_names.iter().zip($joint_values.iter()) {
            writeln!($f, "\t\t{}: {:.3?}", joint_name, joint_value)?;
        }
    };
}

impl fmt::Debug for RealtimeRobotState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let joint_names = [
            "shoulder_pan_joint",
            "shoulder_lift_joint",
            "elbow_joint",
            "wrist_1_joint",
            "wrist_2_joint",
            "wrist_3_joint",
        ];

        let accelerometer_directions = ["X", "Y", "Z"];

        writeln!(f, "Robot State:")?;
        writeln!(f, "\tmessage_size: {}", self.message_size)?;
        writeln!(f, "\ttime: {}", self.time)?;
        write_joints!("q_target", f, joint_names, self.q_target);
        write_joints!("qd_target", f, joint_names, self.qd_target);
        write_joints!("qdd_target", f, joint_names, self.qdd_target);
        write_joints!("i_target", f, joint_names, self.i_target);
        write_joints!("m_target", f, joint_names, self.m_target);
        write_joints!("q_actual", f, joint_names, self.q_actual);
        write_joints!("qd_actual", f, joint_names, self.qd_actual);
        write_joints!("i_actual", f, joint_names, self.i_actual);
        write_joints!("i_control", f, joint_names, self.i_control);
        write_joints!(
            "tool_vector_actual",
            f,
            joint_names,
            self.tool_vector_actual
        );
        write_joints!("tcp_speed_actual", f, joint_names, self.tcp_speed_actual);
        write_joints!("tcp_force", f, joint_names, self.tcp_force);
        write_joints!(
            "tool_vector_target",
            f,
            joint_names,
            self.tool_vector_target
        );
        write_joints!("tcp_speed_target", f, joint_names, self.tcp_speed_target);
        writeln!(f, "\tdigital_input_bits: {}", self.digital_input_bits)?;
        write_joints!(
            "motor_temperatures",
            f,
            joint_names,
            self.motor_temperatures
        );
        writeln!(f, "\tcontroller_timer: {}", self.controller_timer)?;
        writeln!(f, "\ttest_value: {}", self.test_value)?;
        writeln!(f, "\trobot_mode: {:?}", self.robot_mode)?;
        write_joints!("joint_modes", f, joint_names, self.joint_modes);
        writeln!(f, "\tsafety_mode: {:?}", self.safety_mode)?;

        // Assuming this is joint data although it is undocumented
        write_joints!("undocumented_one", f, joint_names, self.undocumented_one);

        // Handle single special case
        writeln!(f, "\ttool_accelerometer_values:")?;
        for (direction, value) in accelerometer_directions
            .iter()
            .zip(self.tool_accelerometer_values.iter())
        {
            writeln!(f, "\t\t{}: {}", direction, value)?;
        }

        // Assuming this is joint data although it is undocumented
        write_joints!("undocumented_two", f, joint_names, self.undocumented_two);
        writeln!(f, "\tspeed_scaling: {:?}", self.speed_scaling)?;
        writeln!(f, "\tlinear_momentum_norm: {:?}", self.linear_momentum_norm)?;
        writeln!(f, "\tundocumented_three: {:?}", self.undocumented_three)?;
        writeln!(f, "\tundocumented_four: {:?}", self.undocumented_four)?;
        writeln!(f, "\tv_main: {:?}", self.v_main)?;
        writeln!(f, "\tv_robot: {:?}", self.v_robot)?;
        writeln!(f, "\ti_robot: {:?}", self.i_robot)?;
        write_joints!("v_actual", f, joint_names, self.v_actual);
        writeln!(f, "\tdigital_outputs: {:?}", self.digital_outputs)?;
        writeln!(f, "\tprogram_state: {:?}", self.program_state)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // A packet captured from the realtime universal robot protocol v3.4.
    pub const REALTIME_PACKET_V3_4: [u8; 1060] = [
        0, 0, 4, 36, 64, 145, 167, 190, 118, 200, 180, 57, 191, 209, 147, 91, 159, 48, 90, 160,
        191, 251, 162, 51, 209, 16, 180, 96, 192, 1, 159, 190, 104, 136, 90, 48, 191, 233, 219, 34,
        162, 33, 104, 192, 63, 249, 133, 135, 160, 0, 0, 0, 191, 159, 190, 116, 68, 45, 24, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 60, 80, 213, 104, 104, 243, 211, 139, 63, 225, 94, 114, 214, 61, 77, 206,
        63, 226, 86, 239, 171, 50, 231, 221, 63, 205, 35, 9, 70, 60, 6, 167, 63, 89, 130, 212, 13,
        226, 175, 65, 0, 0, 0, 0, 0, 0, 0, 0, 60, 139, 160, 208, 205, 136, 128, 0, 64, 28, 129,
        187, 214, 5, 65, 76, 64, 21, 89, 5, 235, 2, 205, 23, 63, 240, 141, 170, 58, 159, 56, 230,
        63, 124, 252, 167, 88, 188, 197, 53, 0, 0, 0, 0, 0, 0, 0, 0, 191, 209, 147, 91, 68, 66,
        209, 128, 191, 251, 162, 51, 209, 16, 180, 96, 192, 1, 159, 190, 104, 136, 90, 48, 191,
        233, 219, 34, 162, 33, 104, 192, 63, 249, 133, 135, 160, 0, 0, 0, 191, 159, 190, 116, 68,
        45, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 80, 213, 104, 96, 0, 0,
        0, 63, 225, 94, 114, 224, 0, 0, 0, 63, 226, 86, 239, 160, 0, 0, 0, 63, 205, 35, 9, 64, 0,
        0, 0, 63, 89, 130, 212, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 80, 213, 104, 96, 0, 0, 0,
        63, 225, 94, 114, 224, 0, 0, 0, 63, 226, 86, 239, 160, 0, 0, 0, 63, 205, 35, 9, 64, 0, 0,
        0, 63, 89, 130, 212, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 205, 157, 109, 147, 153, 118,
        8, 191, 199, 5, 5, 24, 233, 104, 217, 63, 196, 33, 176, 240, 197, 219, 169, 191, 254, 232,
        24, 163, 171, 84, 111, 64, 3, 197, 88, 235, 212, 88, 247, 63, 172, 48, 218, 37, 182, 194,
        199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62, 184, 136, 169, 40, 76, 100,
        182, 190, 179, 29, 132, 77, 186, 12, 77, 190, 161, 138, 148, 102, 225, 184, 147, 62, 117,
        127, 208, 97, 132, 114, 116, 62, 114, 92, 213, 78, 133, 223, 193, 62, 5, 110, 168, 213,
        150, 78, 188, 63, 205, 157, 109, 114, 228, 246, 214, 191, 199, 5, 5, 66, 252, 181, 71, 63,
        196, 33, 176, 240, 197, 219, 169, 191, 254, 232, 24, 135, 85, 194, 121, 64, 3, 197, 88,
        246, 166, 135, 188, 63, 172, 48, 218, 34, 251, 42, 161, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 68, 0, 0, 0, 0, 0, 0, 64, 68, 0, 0, 0, 0, 0,
        0, 64, 68, 0, 0, 0, 0, 0, 0, 64, 68, 0, 0, 0, 0, 0, 0, 64, 68, 0, 0, 0, 0, 0, 0, 64, 68, 0,
        0, 0, 0, 0, 0, 63, 210, 180, 74, 31, 8, 3, 4, 64, 32, 0, 0, 0, 0, 0, 0, 64, 28, 0, 0, 0, 0,
        0, 0, 64, 111, 160, 0, 0, 0, 0, 0, 64, 111, 160, 0, 0, 0, 0, 0, 64, 111, 160, 0, 0, 0, 0,
        0, 64, 111, 160, 0, 0, 0, 0, 0, 64, 111, 160, 0, 0, 0, 0, 0, 64, 111, 160, 0, 0, 0, 0, 0,
        63, 240, 0, 0, 0, 0, 0, 0, 64, 6, 61, 11, 252, 156, 90, 244, 64, 4, 153, 110, 24, 172, 171,
        20, 64, 17, 36, 74, 124, 80, 121, 245, 64, 5, 10, 199, 221, 169, 244, 252, 64, 5, 10, 199,
        221, 169, 244, 252, 64, 5, 10, 199, 221, 169, 244, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        63, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 38, 0, 0, 0, 0, 0, 0, 64, 96, 64, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 72, 0, 0, 0, 0, 0, 0, 64, 5, 133, 115, 64, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 240, 0,
        0, 0, 0, 0, 0,
    ];

    #[test]
    fn from_packet_3_4() {
        RealtimeRobotState::from_packet(&REALTIME_PACKET_V3_4);
    }
}
