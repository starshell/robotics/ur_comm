//! A server for communicating with a Universal Robot.

#![deny(warnings)]

use futures::sync::mpsc;
use futures::{Future, Sink, Stream};
use std::io::{self, Read, Write};
use std::net::{IpAddr, SocketAddr};
use std::thread;
use tokio;

/// The Realtime Universal Robot Server
///
/// Recieves robot state messages on port 30003 at 125 Hz.
pub struct UrServer {
    socket: SocketAddr,
}

impl UrServer {
    /// Create a server for recieving messages from a realtime server.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::net::{IpAddr, Ipv4Addr};
    /// use ur_comm::server::UrServer;
    ///
    /// let server = UrServer::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)));
    /// ```
    pub fn new(ip_address: IpAddr) -> UrServer {
        let socket = SocketAddr::new(ip_address, 30003);
        UrServer { socket }
    }

    pub fn run(&self) {
        // Right now Tokio doesn't support a handle to stdin running on the event
        // loop, so we farm out that work to a separate thread. This thread will
        // read data (with blocking I/O) from stdin and then send it to the event
        // loop over a standard futures channel.
        let (stdin_tx, stdin_rx) = mpsc::unbounded();
        thread::spawn(|| read_stdin(stdin_tx));
        let stdin_rx = stdin_rx.map_err(|_| panic!()); // errors not possible on rx

        // Now that we've got our stdin read we either set up our TCP connection or
        // our UDP connection to get a stream of bytes we're going to emit to
        // stdout.
        let stdout = tcp::connect(&self.socket, Box::new(stdin_rx));

        // And now with our stream of bytes to write to stdout, we execute that in
        // the event loop! Note that this is doing blocking I/O to emit data to
        // stdout, and in general it's a no-no to do that sort of work on the event
        // loop. In this case, though, we know it's ok as the event loop isn't
        // otherwise running anything useful.
        let mut out = io::stdout();

        tokio::run({
            stdout
                .for_each(move |robot_state| {
                    println!("{:?}", &robot_state);
                    out.write_all(b"1")
                })
                .map_err(|e| println!("error reading stdout; error = {:?}", e))
        });
    }
}

mod ur_codec {
    use bytes::{BufMut, BytesMut};
    //use nom;
    use realtime::robot_state::RealtimeRobotState;
    use std::io;
    use tokio_io::codec::{Decoder, Encoder};

    /// A `Codec` implementation that speaks Universal Robot protocol.
    pub struct RealtimeProtocolStream;

    impl Decoder for RealtimeProtocolStream {
        type Item = RealtimeRobotState;
        type Error = io::Error;

        fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
            if buf.len() > 1060 {
                //let len = buf.len();
                Ok(Some(RealtimeRobotState::from_packet(&buf.split_to(1060))))
            } else {
                Ok(None)
            }
        }
    }

    impl Encoder for RealtimeProtocolStream {
        type Item = Vec<u8>;
        type Error = io::Error;

        fn encode(&mut self, data: Vec<u8>, buf: &mut BytesMut) -> io::Result<()> {
            buf.put(&data[..]);
            Ok(())
        }
    }
}

mod tcp {
    use std::io;
    use std::net::SocketAddr;

    use tokio;
    use tokio::prelude::*;
    use tokio_io::codec::Decoder;

    use realtime::robot_state::RealtimeRobotState;
    use server::server::ur_codec::RealtimeProtocolStream;

    pub fn connect(
        addr: &SocketAddr,
        stdin: Box<tokio::prelude::Stream<Item = Vec<u8>, Error = io::Error> + Send>,
    ) -> Box<tokio::prelude::Stream<Item = RealtimeRobotState, Error = io::Error> + Send> {
        let tcp = tokio::net::TcpStream::connect(addr);

        // After the TCP connection has been established, we set up our client
        // to start forwarding data.
        //
        // First we use the `Io::framed` method with a simple implementation of
        // a `Codec`. We then split that in two to work with the stream and
        // sink separately.
        //
        // Half of the work we're going to do is to take all data we receive on
        // `stdin` and send that along the TCP stream (`sink`). The second half
        // is to take all the data we receive (`stream`) and then write that to
        // stdout. We'll be passing this handle back out from this method.
        //
        // You'll also note that we *spawn* the work to read stdin and write it
        // to the TCP stream. This is done to ensure that happens concurrently
        // with us reading data from the stream.
        Box::new(tcp.map(move |stream| {
            let (sink, stream) = RealtimeProtocolStream.framed(stream).split();

            tokio::spawn(stdin.forward(sink).then(|result| {
                if let Err(e) = result {
                    panic!("failed to write to socket: {}", e)
                }
                Ok(())
            }));

            stream
        }).flatten_stream())
    }
}

// Helper method which will read data from stdin and send it along the
// sender provided.
fn read_stdin(mut tx: mpsc::UnboundedSender<Vec<u8>>) {
    let mut stdin = io::stdin();
    loop {
        let mut buf = vec![0; 1024];
        let n = match stdin.read(&mut buf) {
            Err(_) | Ok(0) => break,
            Ok(n) => n,
        };
        buf.truncate(n);
        tx = match tx.send(buf).wait() {
            Ok(tx) => tx,
            Err(_) => break,
        };
    }
}
