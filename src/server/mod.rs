//! A communication server for the Universal Robot.

#![deny(warnings)]

mod server;

pub use self::server::UrServer;

/// The client types that are available.
pub enum ClientType {
    /// Transmits Robot State and additional messages, recieves URScript commands.
    Primary,

    /// Transmits Robot State messages, recieves URScript commands.
    ///
    /// Universal Robots introduced breaking changes in firmware
    /// patch on 3.1 after the patch it will send one Robot Message
    /// followed by only Robot State messages. For discussion see
    /// [Fatal error when unpacking RobotState packet #183](https://github.com/ros-industrial/universal_robot/issues/183).
    Secondary,

    /// Transmits Robot State messages, recieves URScript commands.
    RealTime,

    /// Transmits Robot State messages, recieves various data.
    Rtde,
}

/// Specifies the lifetime of data recieved by the `UrServer`.
///
/// This is useful for configuring the `UrServer` for different types
/// of applications or debugging. For example a UR driver in production
/// would likely want to run using `VOLATILE` since only the most recent
/// robot state is relevant, but a robot state logger would want to run
/// with `BUFFERED` so that all desired data points get logged.
pub enum Durability {
    /// Once data is published it is not maintained.
    VOLATILE,

    /// Stores data locally in memory.
    BUFFERED,

    /// Stores data persistently on disk.
    PERSISTENT,
}
