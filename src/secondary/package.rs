//! The primary and secondary clients packages.
//!
//! ```
//! +-----------------------+
//! |                       |
//! | Length of package     | 4 bytes (int)
//! |                       |
//! +-----------------------+
//! | Robot Message Type    | 1 byte (uchar)
//! +-----------------------+
//! |                       |
//! | Length of sub-package | 4 bytes (int)
//! |                       |
//! +-----------------------+
//! | Package Type          | 1 byte (uchar)
//! +-----------------------+
//! |                       |
//! | Content...            | n bytes
//! |                       |
//! +-----------------------+
//! |                       |
//! | Length of sub-package | 4 bytes
//! |                       |
//! +-----------------------+
//! | Package Type          | 1 byte
//! +-----------------------+
//! |                       |
//! | Content...            | n bytes
//! |                       |
//! +-----------------------+
//! |                       |
//! |                       |
//! +                       +
//! ```

/// A complete package.
pub struct Package {
    length: i16,
    message_type: u8,
}

/// The type of the robot message.
pub enum MessageType {
    RobotState = 16,
    RobotMessage = 20,
    ProgramStateMessage = 25,
}
