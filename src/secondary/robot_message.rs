//! A robot message type.

enum MessageType {
    Text = 0,
    ProgramLabel = 1,
    ProgramStateMessageVariableUpdate = 2,
    Version = 3,
    SafetyMode = 5,
    ErrorCode = 6,
    Key = 7,
    RequestValue = 9,
    RuntimeException = 10,
}

pub struct MessagePacket {
    timestamp: u64,
    source: u8,
}

pub struct VersionMessage {
    project_name: String,
    major_version: u8,
    minor_version: u8,
    svn_version: i32,
    build_date: String,
}
