//! A robot state message.

/// Possible types of data packages.
enum PackageType {
    RobotModeData = 0,
    JointData = 1,
    ToolData = 2,
    MasterboardData = 3,
    CartesianInfo = 4,
    KinematicsInfo = 5,
    ConfigurationData = 6,
    ForceModeData = 7,
    AdditionalInfo = 8,
    CalibrationData = 9,
    SafetyData = 10,
}

struct RobotModeData {
    package_length: i16,
    package_type: u8,
    timestamp: u64,
    physical_robot_connected: bool,
    real_robot_enabled: bool,
    robot_power_on: bool,
    emergency_stopped: bool,
    protective_stopped: bool,
    program_running: bool,
    program_paused: bool,
    robot_mode: u8,
    control_mode: u8,
    target_speed_fraction: f64,
    speed_scaling: f64,
    target_speed_fraction_limit: f64,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
