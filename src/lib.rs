/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! Universal Robot communication protocols.
//!
//! Only supports firmware versions from 3.
//!
//! # References
//!
//! * [Universal Robots - Remote Control Via TCP/IP - 16496](https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/remote-control-via-tcpip-16496/)
//! * [Universal Robots - Real-Time Data Exchange (RTDE) Guide - 22229](https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/real-time-data-exchange-rtde-guide-22229/)

#[macro_use]
extern crate nom;
extern crate bytes;
extern crate futures;
extern crate tokio;
extern crate tokio_core;
extern crate tokio_io;

mod common;
mod realtime;
pub mod server;

pub use common::modes;
