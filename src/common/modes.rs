//! Mode Messages.
//!
//! These enumerations are used across protocols.

/// The current mode of the robot.
#[derive(Debug, Clone)]
pub enum RobotMode {
    Disconnected = 0,
    ConfirmSafety = 1,
    Booting = 2,
    PowerOff = 3,
    PowerOn = 4,
    Idle = 5,
    Backdrive = 6,
    Running = 7,
    UpdatingFirmware = 8,
    UnknownState,
}

impl RobotMode {
    pub fn from_f64(value: f64) -> RobotMode {
        match value.trunc() as u64 {
            0 => RobotMode::Disconnected,
            1 => RobotMode::ConfirmSafety,
            2 => RobotMode::Booting,
            3 => RobotMode::PowerOff,
            4 => RobotMode::PowerOn,
            5 => RobotMode::Idle,
            6 => RobotMode::Backdrive,
            7 => RobotMode::Running,
            8 => RobotMode::UpdatingFirmware,
            _ => RobotMode::UnknownState,
        }
    }
}

/// The current mode of a joint.
#[derive(Debug, Clone)]
pub enum JointMode {
    ShuttingDown = 236,
    PartDCalibration = 237,
    Backdrive = 238,
    PowerOff = 239,
    NotResponding = 245,
    MotorInitialisation = 246,
    Booting = 247,
    PartDCalibrationError = 248,
    Bootloader = 249,
    Calibration = 250,
    Fault = 252,
    Running = 253,
    Idle = 255,
    UnknownState,
}

impl JointMode {
    pub fn from_f64(value: f64) -> JointMode {
        match value.trunc() as u64 {
            236 => JointMode::ShuttingDown,
            237 => JointMode::PartDCalibration,
            238 => JointMode::Backdrive,
            239 => JointMode::PowerOff,
            245 => JointMode::NotResponding,
            246 => JointMode::MotorInitialisation,
            247 => JointMode::Booting,
            248 => JointMode::PartDCalibrationError,
            249 => JointMode::Bootloader,
            250 => JointMode::Calibration,
            252 => JointMode::Fault,
            253 => JointMode::Running,
            255 => JointMode::Idle,
            _ => JointMode::UnknownState,
        }
    }
}

/// The current mode of the tool.
#[derive(Debug, Clone)]
pub enum ToolMode {
    Bootloader = 249,
    Running = 253,
    Idle = 255,
    UnknownState,
}

impl ToolMode {
    pub fn from_f64(value: f64) -> ToolMode {
        match value.trunc() as u64 {
            249 => ToolMode::Bootloader,
            253 => ToolMode::Running,
            255 => ToolMode::Idle,
            _ => ToolMode::UnknownState,
        }
    }
}

/// The current safetly status.
#[derive(Debug, Clone)]
pub enum SafetyMode {
    Normal = 1,
    Reduced = 2,
    ProtectiveStop = 3,
    Recovery = 4,
    /// (SI0 + SI1 + SBUS) Physical s-stop interface input.
    SafeguardStop = 5,
    /// (EA + EB + SBUS->Euromap67) Physical e-stop interface input activated.
    SystemEmergencyStop = 6,
    /// (EA + EB + SBUS->Screen) Physical e-stop interface input activated.
    RobotEmergencyStop = 7,
    ModeViolation = 8,
    Fault = 9,
    UnknownState,
}

impl SafetyMode {
    pub fn from_f64(value: f64) -> SafetyMode {
        match value.trunc() as u64 {
            1 => SafetyMode::Normal,
            2 => SafetyMode::Reduced,
            3 => SafetyMode::ProtectiveStop,
            4 => SafetyMode::Recovery,
            5 => SafetyMode::SafeguardStop,
            6 => SafetyMode::SystemEmergencyStop,
            7 => SafetyMode::RobotEmergencyStop,
            8 => SafetyMode::ModeViolation,
            9 => SafetyMode::Fault,
            _ => SafetyMode::UnknownState,
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
