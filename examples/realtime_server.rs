/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

//! And example of running the realtime universal robot server.

extern crate ur_comm;

use std::net::{IpAddr, Ipv4Addr};
use ur_comm::server::UrServer;

fn main() {
    let ip_address = IpAddr::V4(Ipv4Addr::new(172, 17, 0, 2));
    let realtime_server = UrServer::new(ip_address);
    realtime_server.run();
}
